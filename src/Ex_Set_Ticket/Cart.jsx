import React, { Component } from "react";
import { connect } from "react-redux";
import { cancelTickets, setTickets } from "./redux/actions/ticketActions";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.soGhe}</td>
          <td>{item.gia.toLocaleString()}</td>
          <td>
            <button
              className=" btn btn-danger"
              onClick={() => {
                this.props.handleCancelTickets(item.soGhe);
              }}
              style={{ fontSize: 10 }}
            >
              Cancel
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="mt-4">
        <div className="text-left">
          <button className="gheDuocDat"></button>
          <span style={{ fontSize: 25 }} className="text-warning ml-2">
            Ghế đã đặt
          </span>
          <br />
          <button className="gheDangDat"></button>
          <span style={{ fontSize: 25 }} className="text-success ml-2">
            Ghế đang đặt
          </span>
          <br />
          <button className="gheChuaDat"></button>
          <span style={{ fontSize: 25 }} className="text-light ml-2">
            Ghế chưa đặt
          </span>
        </div>
        <div className="mt-3">
          <table className="table text-light bg__form">
            <thead>
              <tr>
                <th>Vị trí ngồi</th>
                <th>Giá vé</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>{this.renderTbody()}</tbody>
          </table>
        </div>
        <div >
          <span className="d-flex text-warning">Tổng thanh toán : {this.props.cart.reduce((total,item)=>{
            return (total += item.gia);
          },0).toLocaleString()} vnđ </span>
        <button
          onClick={this.props.handleSetTickets}
          className="btn btn-success"
        >
          ĐẶT VÉ - THANH TOÁN
        </button>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.tiketReducer.bookSeats,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleCancelTickets: (seat) => {
      dispatch(cancelTickets(seat));
    },
    handleSetTickets: () => {
      dispatch(setTickets());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
