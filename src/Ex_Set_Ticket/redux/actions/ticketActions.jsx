import { BOOK_TICKETS, CANCEL_TICKETS, SET_TICKETS } from './../constants/ticketsConstants';


export const bookTickets=(seat)=>{
    return {
        type:BOOK_TICKETS,
        payload:seat,
    }
}
export const cancelTickets=(seat)=>{
    return {
        type:CANCEL_TICKETS,
        payload:seat,
    }
}
export const setTickets=()=>{
    return {
        type:SET_TICKETS,
    }
}