import { data_seat } from "../../danhSachGhe";
import {
  BOOK_TICKETS,
  CANCEL_TICKETS,
  SET_TICKETS,
} from "./../constants/ticketsConstants";
let initialState = {
  dataSeat: data_seat,
  bookSeats: [],
};
export const tiketReducer = (state = initialState, action) => {
  switch (action.type) {
    case BOOK_TICKETS: {
      let newSeats = [...state.bookSeats];
      let index = newSeats.findIndex(
        (seat) => seat.soGhe == action.payload.soGhe
      );
      if (index == -1) {
        newSeats.push(action.payload);
      } else {
        newSeats.splice(index, 1);
      }
      state.bookSeats = newSeats;
      return { ...state };
    }
    case CANCEL_TICKETS: {
      let newSeats = [...state.bookSeats];
      let index = newSeats.findIndex((seat) => (seat.soGhe = action.payload));
      if (index !== -1) {
        newSeats.splice(index, 1);
      }
      state.bookSeats = newSeats;
      return { ...state };
    }
    case SET_TICKETS: {
      let newSeats = [...state.bookSeats];
      let newDataSeat = [...state.dataSeat];

      newDataSeat.forEach((dataSeat) => {
        dataSeat.danhSachGhe.forEach((seat) => {
          state.bookSeats.forEach((bookSeat) => {
            if (bookSeat.soGhe == seat.soGhe) {
              seat.daDat = true;
            }
          });
        });
      });
      state.dataSeat = newDataSeat;
      newSeats = [];
      state.bookSeats = newSeats;
      return { ...state };
    }
    default:
      return state;
  }
};
