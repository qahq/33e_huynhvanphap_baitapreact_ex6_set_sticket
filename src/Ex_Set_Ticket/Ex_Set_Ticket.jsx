import React, { Component } from "react";
import "./ticket.css";
import bgTicket from "../assets/bgmovie.jpg";
import RowofSeats from "./RowofSeats";
import Cart from "./Cart";
import { connect } from "react-redux";

class Ex_Set_Ticket extends Component {
  renderContent = () => {
    return this.props.dataSeat.map((hangGhe, index) => {
      return <RowofSeats hangGhe={hangGhe} soHang={index} key={index} />;
    });
  };
  render() {
    return (
      <div
        className="bg_Ticket"
        style={{
          backgroundImage: `url(${bgTicket})`,
          width: "100%",
          height: "100%",
        }}
      >
        <div
          style={{
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0,0,0,0.7)",
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-7 text-center ">
                <h1 className="text-warning mt-3 ">
                  ĐẶT VÉ XEM PHIM RẠP NHÀ PHÁP
                </h1>
                <h2
                  style={{ fontSize: 18 }}
                  className="text-white text-center "
                >
                  Màn hình
                </h2>

                <div
                  className="mt-2"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  }}
                >
                  <div className="roof ml-5 mt-3"></div>
                  {this.renderContent()}
                </div>
              </div>
              <div className="col-5">
                <h3 className="text-light mt-5">Danh sách ghế bạn chọn</h3>
                <Cart />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    dataSeat: state.tiketReducer.dataSeat,
  };
};

export default connect(mapStateToProps)(Ex_Set_Ticket);
