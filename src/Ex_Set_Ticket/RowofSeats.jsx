import React, { Component } from "react";
import { connect } from "react-redux";
import { bookTickets } from "./redux/actions/ticketActions";

class RowofSeats extends Component {
  renderSeats = () => {
    return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
      let gheDuocChon = "";
      let gheDangChon = "";
      let disabled = false;
      if (ghe.daDat == true) {
        gheDuocChon = "gheDuocDat";
        disabled = true;
      }
      let seatId = this.props.bookSeats.findIndex(
        (idSeat) => ghe.soGhe == idSeat.soGhe
      );
      if (seatId !== -1) {
        gheDangChon = "gheDangDat";
      }

      return (
        <button
          style={{ marginLeft: 10, textAlign: "center" }}
          onClick={() => {
            this.props.handleBookTickets(ghe);
          }}
          className={`gheChuaDat ${gheDuocChon}${gheDangChon}`}
          disabled={disabled}
          key={index}
        >
          {ghe.soGhe}
        </button>
      );
    });
  };
  renderRow = () => {
    return this.props.hangGhe.danhSachGhe.map((hang, index) => {
      return (
        <button className="rowSo" key={index}>
          {hang.soGhe}
        </button>
      );
    });
  };
  renderRowofSeats = () => {
    if (this.props.soHang == 0)
      return (
        <div className="" style={{ fontSize: 5 }}>
          {this.props.hangGhe.hang}
          {this.renderRow()}
        </div>
      );
    else {
      return (
        <div className="">
          {this.props.hangGhe.hang}
          {this.renderSeats()}
        </div>
      );
    }
  };
  render() {
    return (
      <div
        className="text-warning text-center mt-2   "
        style={{ fontSize: "20px" }}
      >
        {this.renderRowofSeats()}
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    bookSeats: state.tiketReducer.bookSeats,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleBookTickets: (seat) => {
      dispatch(bookTickets(seat));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RowofSeats);
